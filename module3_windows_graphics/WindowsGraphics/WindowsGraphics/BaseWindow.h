#pragma once
#include <Windows.h>

template<class T>
class BaseWindow
{
public:
	BaseWindow();
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	BOOL Create(
		PCWSTR lpWindowName,
		DWORD dwStyle,
		DWORD dwExStyle = 0,
		int x = CW_USEDEFAULT,
		int y = CW_USEDEFAULT,
		int nWidth = CW_USEDEFAULT,
		int nHeight = CW_USEDEFAULT,
		HWND hWndParent = 0,
		HMENU hMenu = 0
	);
	HWND Window() const;
protected:
	HWND m_hwnd;
	virtual PCWSTR ClassName() const = 0;
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;
};

template<class T>
BaseWindow<T>::BaseWindow() : m_hwnd(NULL) {}

template<class WINDOW_TYPE>
LRESULT BaseWindow<WINDOW_TYPE>::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	WINDOW_TYPE* pThis = NULL;
	if (uMsg == WM_NCCREATE)
	{
		OutputDebugString(L"WM_NCCREATE\n");
		CREATESTRUCT* PCreate = (CREATESTRUCT*)lParam;
		pThis = (WINDOW_TYPE*)PCreate->lpCreateParams;
		SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)pThis);
		pThis->m_hwnd = hwnd;
	}
	else
	{
		pThis = (WINDOW_TYPE*)GetWindowLongPtr(hwnd, GWL_USERDATA);
	}
	if (pThis) {
		return pThis->HandleMessage(uMsg, wParam, lParam);
	}
	else {
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
	return 0;
}

template<class WINDOW_TYPE>
BOOL BaseWindow<WINDOW_TYPE>::Create(PCWSTR lpWindowName, DWORD dwStyle, DWORD dwExStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu)
{
	WNDCLASS wc = {};
	wc.lpfnWndProc = WINDOW_TYPE::WindowProc;
	wc.hInstance = GetModuleHandle(NULL);
	wc.lpszClassName = ClassName();

	RegisterClass(&wc);

	m_hwnd = CreateWindowEx(
		dwExStyle,
		ClassName(),
		lpWindowName,
		dwStyle,
		x, y, nWidth, nHeight,
		hWndParent, hMenu, GetModuleHandle(NULL), this
	);
	return (m_hwnd ? TRUE : FALSE);
}

template<class WINDOW_TYPE>
HWND BaseWindow<WINDOW_TYPE>::Window() const
{
	return m_hwnd;
}
